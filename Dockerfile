
FROM openjdk:17
EXPOSE 5000
ADD target/tweetapp-0.0.1-SNAPSHOT.jar tweetapp-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","tweetapp-0.0.1-SNAPSHOT.jar"]