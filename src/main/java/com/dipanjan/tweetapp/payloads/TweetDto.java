package com.dipanjan.tweetapp.payloads;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TweetDto {
    private Integer id;

    @NotEmpty
    @Size(min = 10, message ="tweet description must be minimum of 10 characters")
    private String description;

    private String createdAtDate;

    private String lastUpdatedDate;

    private UserDto user;

    private List<CommentDto> comments;

}
